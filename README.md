# Kthreeyboard

A handwired split mechanical keyboard. This repo contains the models, the firmware can be found at [https://gitlab.com/TheZoq2/kthreeyboard_firmware](TheZoq2/khtreeyboard)

![Keyboard installed at work](images/at_work.jpg "opt title")
![Keyboard installed at work](images/backlight.jpg "opt title")
![Keyboard installed at work](images/bright_work.jpg "opt title")
