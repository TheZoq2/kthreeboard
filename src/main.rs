use scad::*;

use scad_util::{
    constants::{x_axis, y_axis, z_axis},
    compositions::object_at_corners,
};

use nalgebra::Vector2;

fn usb_cable_shape(length: f32, extra_radius: f32) -> ScadObject {
    let radius = 2. + extra_radius;
    let x_distance = 8.;
    let y_distance = 2.;
    let cylinders = scad!(Cylinder(length, Radius(radius)));
    scad!(Hull; {
        object_at_corners(x_axis(), y_axis(), x_distance, y_distance, cylinders)
    })
}

fn usb_c_port() -> ScadObject {
    // let length = 9.36;
    let length = 12.36;
    let width = 9.0;
    let height = 3.6;

    // let pcb_offset = 3.0;
    let pcb_offset = 5.0;

    let cylinder = scad!(Rotate(-90., x_axis()); {
        scad!(Cylinder(length, Diameter(height)))
    });

    let x_offset = (width-height) / 2.;
    let y_offset = length - pcb_offset;
    let z_offset = height/2.;
    scad!(Hull; {
        scad!(Translate(vec3(x_offset, -y_offset, z_offset)); cylinder.clone()),
        scad!(Translate(vec3(-x_offset, -y_offset, z_offset)); cylinder.clone())
    })
}

fn micro_usb_port() -> ScadObject {
    // let length = 9.36;
    let length = 8.36;
    let width = 9.0;
    let height = 3.2;

    // let pcb_offset = 3.0;
    let pcb_offset = 5.0;

    let cylinder = scad!(Rotate(-90., x_axis()); {
        scad!(Cylinder(length, Diameter(height)))
    });

    let x_offset = (width-height) / 2.;
    let y_offset = length - pcb_offset;
    let z_offset = height/2.;
    scad!(Hull; {
        scad!(Translate(vec3(x_offset, -y_offset, z_offset)); cylinder.clone()),
        scad!(Translate(vec3(-x_offset, -y_offset, z_offset)); cylinder.clone())
    })
}

fn inter_board_connector() -> ScadObject {
    let radius_padding = 0.4; 
    let width = 20.5;
    let length = 10.;
    let thickness = 1.1;
    let radius = 2.;

    let pcb = scad!(Hull; {
        object_at_corners(
            x_axis(),
            y_axis(),
            width - radius*2.,
            length - radius*2.,
            scad!(Cylinder(thickness, Radius(radius + radius_padding)))
        )
    });

    scad!(Union; {
        scad!(NamedColor("Blue".into()); pcb),
        scad!(Translate(vec3(0., length/2., thickness)); micro_usb_port())
    })
}

fn microcontroller() -> ScadObject {
    let width = 21.3;
    let length = 51.4;
    let thickness = 2.14;
    let extra_thickness = 1.;

    let pcb = centered_cube(vec3(width, length, thickness + extra_thickness), (true, true, false));

    scad!(Union; {
        scad!(NamedColor("Purple".into()); pcb),
        scad!(Translate(vec3(0., length/2., thickness)); usb_c_port())
    })
}

fn switch() -> ScadObject {
    scad!(Rotate(90., x_axis()); 
        scad!(Import("cherrymx.stl".into()))
    )
}

qstruct!(Keyboard() {
    keycap_size: f32 = 19.,
    thickness: f32 = 3.,
    top_thickness: f32 = 3.0,
    bottom_plate_thickness: f32 = 2.1,
    inner_height: f32 = 10.,
});

impl Keyboard {
    fn switch_locations_2d(&self) -> Vec<(Vector2<f32>, f32)> {
        let mut result = vec![];

        let columns = vec![
            (4, -0.10),
            (4, -0.20),
            (4, -0.30),
            (3, -0.40),
            (3, -0.35),
            (3, -0.25),
        ];

        // Keys in the normal columns
        for (col, (key_count, offset)) in columns.into_iter().enumerate() {
            for row in 0..key_count {
                let pos =
                    vec2(col as f32, -(row as f32 + offset))
                        * self.keycap_size;

                result.push((pos, 0.));
                // result.add_child(scad!(Translate2d(pos); object.clone()));
            }
        }

        let extra_keys = vec!(
                (vec2(3.58, 0.29 - 2.97), -4.),
                (vec2(4.65, 0.29 - 3.1), -10.),
                (vec2(5.7, 0.29 - 3.35), -17.),
            );

        for (pos, angle) in extra_keys {
            result.push((pos * self.keycap_size, angle));
        }

        result
    }
    fn object_at_switch_locations_2d(&self, object: ScadObject) -> ScadObject{
        let mut result = scad!(Union);

        for (pos, angle) in self.switch_locations_2d() {
            result.add_child(scad!(Translate2d(pos); {
                scad!(Rotate2d(angle); object.clone())
            }))
        }

        result
    }

    fn object_at_switch_locations_3d(&self, object: ScadObject, z_offset: f32) -> ScadObject {
        let mut result = scad!(Union);

        for (pos, angle) in self.switch_locations_2d() {
            let pos = vec3(pos.x, pos.y, z_offset);
            result.add_child(scad!(Translate(pos); {
                scad!(Rotate(angle, z_axis()); object.clone())
            }))
        }

        result
    }

    /**
      Creates the grid of switch holes. The shape looks like this

      ```
      #/*
      ______           ______                             ^
         __|           |__     <- Switch mount Thickness  | total_thickness
      ___|               |___                             v
           |--Inner w--|
                       |-| <- HoleExtraSize
      #*/
      ```
    */
    fn switch_grid(&self, total_thickness: f32, hole_extra_size: f32) -> ScadObject {
        let padding = 0.10;
        let inner_width = 14. + padding * 2.;
        let total_width = inner_width + hole_extra_size * 2.;
        let switch_mount_thickness = 1.3;
        let cutout_height = self.inner_height - self.bottom_plate_thickness;

        let switch_extrude_params = LinExtrudeParams{
            height: total_thickness,
            .. Default::default()
        };
        let full_extrude_params = LinExtrudeParams{
            height: total_thickness - switch_mount_thickness + cutout_height,
            .. Default::default()
        };

        scad!(Union; {
            scad!(LinearExtrude(switch_extrude_params); {
                self.object_at_switch_locations_2d(
                    centered_square(vec2(inner_width, inner_width), (true, true))
                )
            }),
            scad!(Translate(vec3(0., 0., -cutout_height)); {
                scad!(LinearExtrude(full_extrude_params); {
                    self.object_at_switch_locations_2d(
                        centered_square(vec2(total_width, total_width), (true, true))
                    )
                })
            })
        })
    }

    fn keycap_area(&self) -> ScadObject {
        self.object_at_switch_locations_2d(
            centered_square(vec2(self.keycap_size, self.keycap_size), (true, true))
        )
    }

    fn bottom_plate_screwholes(&self, height: f32) -> ScadObject {
        let screwhole_diameter = 3.;
        let size = screwhole_diameter + 3.;

        let locations = vec![
            vec2(-6., -6.),
            vec2(-6., -50.),
            vec2(102.5, -5.),
            vec2(102.5, -35.),
            vec2(56., 13.0),
            vec2(33., -59.),
            vec2(74., -59.),
        ];

        let mut result = scad!(Union);
        for pos in locations {
            result.add_child(
                scad!(Translate(vec3(pos.x, pos.y, 0.)); {
                    scad!(Difference; {
                        centered_cube(vec3(size, size, height), (true, true, false)),
                        scad!(Cylinder(height, Diameter(screwhole_diameter)))
                    }),
                })
            );
        }

        result
    }

    fn outer_shape(&self) -> ScadObject {
        scad!(Translate2d(vec2(-18.5/2., -108.)); {
            // scad!(Offset(OffsetType::Delta(5.), true); {
                // scad!(Offset(OffsetType::Delta(-3.), true); {
                    // self.keycap_area()
                    scad!(Import("top_outline.svg".into()))
                // })
            // })
        })
    }


    fn body(&self) -> ScadObject {
        let chamfer_size = 3.;
        let inner_height = self.inner_height;
        let height = inner_height + self.top_thickness;

        let outer_extrude_params = LinExtrudeParams{
            height: height-chamfer_size/2.,
            .. Default::default()
        };

        let pyramid_size = chamfer_size / 4. * 3.;
        let cube_size = chamfer_size / 4.;
        let outer = scad!(Minkowski; {
            scad!(LinearExtrude(outer_extrude_params);
                scad!(Offset(OffsetType::Delta(-chamfer_size/2. + self.thickness), true); self.outer_shape())
            ),
            scad!(Scale(pyramid_size * vec3(1., 1., 1.)); pyramid()),
            centered_cube(cube_size * vec3(1., 1., 1.), (true, true, true))
        });

        let inner_extrude_params = LinExtrudeParams {
            height: inner_height * 2.,
            center: true,
            .. Default::default()
        };

        let inner_cutout = scad!(LinearExtrude(inner_extrude_params); {
            // scad!(Offset(OffsetType::Delta(-self.thickness), true); {
                self.outer_shape()
            // })
        });

        let switch_holes = self.switch_grid(self.top_thickness, 1.0);

        let microcontroller_holder = {
            let outer = centered_cube(vec3(15., 5., inner_height), (true, false, false));
            scad!(Union; {
                // scad!(Translate(vec3(9., -9., self.bottom_plate_thickness)); outer.clone()),
                scad!(Translate(vec3(9., -42., self.bottom_plate_thickness)); outer)
            })
        };

        let interconnect_holder = {
            let outer = centered_cube(vec3(22., 5., inner_height), (true, false, false));
            scad!(Union; {
                // scad!(Translate(vec3(9., -9., self.bottom_plate_thickness)); outer.clone()),
                scad!(Translate(vec3(95., 3., self.bottom_plate_thickness)); outer)
            })
        };

        scad!(Union; {
            scad!(Difference; {
                scad!(Union; {
                    scad!(Difference; {
                        outer,
                        inner_cutout,
                    }),
                    microcontroller_holder,
                    interconnect_holder
                }),
                scad!(Translate(vec3(0., 0., inner_height)); switch_holes)
            }),
            scad!(Translate(vec3(0., 0., self.bottom_plate_thickness)); {
                self.bottom_plate_screwholes(4.)
            }),
        })
    }
}

fn pyramid() -> ScadObject {
    let points = vec![
        vec3(0., 0., 0.5),
        vec3(0.5, 0., 0.),
        vec3(-0.5, 0., 0.),
        vec3(0., -0.5, 0.),
        vec3(0., 0.5, 0.),
    ];

    let faces = vec![
        vec![0, 4, 1],
        vec![0, 2, 4],
        vec![0, 1, 3],
        vec![0, 3, 2],
        vec![1, 2, 3],
        vec![1, 4, 2]
    ];

    scad!(Polyhedron(points, faces))
}

fn main() {
    let mut scad_file = ScadFile::new();

    scad_file.set_detail(50);

    let board = Keyboard::new();
    let plate_thickness = board.bottom_plate_thickness;

    let interconnect_port = scad!(
        Translate(vec3(95., 9., plate_thickness)); inter_board_connector()
    );
    let microcontroller_transform = |o| {
        scad!(Translate(vec3(9., -15., plate_thickness)); o)
    };
    let microcontroller = microcontroller_transform(microcontroller());

    let cable_pad_length = 1.;
    let cable_pad = scad!(Translate(vec3(9., 14.0, 2.2 + 4.)); {
        scad!(Rotate(90., x_axis()); usb_cable_shape(cable_pad_length, 0.5))
    });
    let cable_cutout= scad!(Translate(vec3(9., 14.0 + 5., 2.2 + 4.)); {
        scad!(Rotate(90., x_axis()); usb_cable_shape(5., 0.5))
    });

    let debug_probe_hole = {
        let shape = centered_cube(vec3(10.3, 10., 2.56), (true, false, false));
        scad!(Translate(vec3(0., -70., plate_thickness)); shape)//.important()
    };

    scad_file.add_object(
        // board.outer_shape()
        scad!(Intersection; {
            scad!(Difference; {
                scad!(Union; {
                    board.body(),
                    cable_pad
                }),
                scad!(Translate(vec3(0., 0.5, 0.)); microcontroller.clone()),
                scad!(Translate(vec3(0., 0.5, 0.)); interconnect_port),
                cable_cutout,
                debug_probe_hole
                // board.object_at_switch_locations_3d(switch(), 13.),
            }),
            /*
            scad!(Translate(vec3(90., 0., 0.)); {
                centered_cube(vec3(40., 50., 30.), (true, true, false))
            })
            */
        })
    );
    /*
    scad_file.add_object(
                scad!(Translate(vec3(0., 0.5, 0.)); microcontroller),
                );
    */
    // scad_file.add_object(board.keycap_area());

    scad_file.write_to_file("out.scad".into());
}
